import datetime
import math
import random
import time

#Entrega date con formato para log
def getDateBridgeFormat():
        fecha = datetime.datetime.now()
        dia = str(fecha.day)
        mes = str(fecha.month)
        anio = str(fecha.year)
        hora = str(fecha.hour)
        minuto = str(fecha.minute)
        segundo = str(fecha.second)
        milisegundo = str(fecha.microsecond)

        #Agrega 0s
        if len(dia) ==1:
            dia = "0" + dia

        if len(mes) ==1:
            mes = "0" + mes

        if len(hora) ==1:
            hora = "0" + hora

        if len(minuto) ==1:
            minuto = "0" + minuto

        if len(segundo) ==1:
            segundo = "0" + segundo

        if len(milisegundo) ==1:
            milisegundo = milisegundo + "00"
        elif len (milisegundo) ==2:
            milisegundo = milisegundo + "0"
        elif len(milisegundo)>3:
            i=0
            nmiliseg= ""
            for i in range(3):
                nmiliseg+= milisegundo[i]
                i+=1
            milisegundo=nmiliseg
            
        return ""+anio+""+mes+""+dia+"|"+hora+minuto+segundo+"."+milisegundo


def createImei(largo):
        i = 0
        imei = ""
        
        while (i < largo):
                imei += str(int(math.floor(random.random()*10)))
                i+=1
        return imei

#Numero de 0 a 9
def getSeconds():
        return math.floor(random.random()*10)


#Entrega time para nombre archivo .qry
def timeFile():
        fecha = datetime.datetime.now()
        dia = str(fecha.day)
        mes = str(fecha.month)
        anio = str(fecha.year)
        hora = str(fecha.hour)
        minuto = str(fecha.minute)
        segundo = str(fecha.second)
        milisegundo = str(fecha.microsecond)

        #Agrega 0s
        if len(dia) ==1:
            dia = "0" + dia

        if len(mes) ==1:
            mes = "0" + mes

        if len(hora) ==1:
            hora = "0" + hora

        if len(minuto) ==1:
            minuto = "0" + minuto

        if len(segundo) ==1:
            segundo = "0" + segundo

        return ""+anio+""+mes+""+dia+hora+minuto+segundo

#Entrega nombre ruta de destino del archivo
#Ruta default: /opt/sixlabs/var/log
def nameRuta(path):
        if (len(path)==0): #no se entrega directorio
                return '/opt/sixlabs/var/log/LogQry_'+timeFile() + '.qry' #default 
                #Agregar cambio de archivo segun hora
        else:
                return path + '/LogQry_' + timeFile() + '.qry'
                
#Programa principal
def creadorQry(path):
    #Abre file
    file = open(nameRuta(path), "w+") 

    i = 0
    random.seed(3) #Misma semilla para cada random
    timeout= time.time() + 20 #Cada 20 seg se abre nuevo archivo
    timeSalida = time.time() + 60 #Programa dura 60 segundos

    while(time.time() <= timeSalida):
        time.sleep(0.1)
        binaryName = "bridge_4"
        date = getDateBridgeFormat()
        plName="check_imei_db"
        callId=createImei(19) #hexa
        protoId=int(math.floor(random.random() * 3) + 5) #5 a 7
        imei=createImei(14) #numero gigante
        status = int(math.floor(random.random() * 4) - 1) #-1 a 2
        error = int(math.floor(random.random() * 11) - 1) #-1 a 2
        
        
        log = binaryName+","+str(date)+","+plName+","+callId+","+str(protoId)+","+imei+","+str(status)+","+str(error)
        print(log)
        
        file.write(log +'\n') #escribe cada log en una linea
        
        i+=1
        
        #Cerrar file actual y crear uno nuevo
        if time.time()>timeout:
            file.close() #Cierra file
            print("finalizado logCreator")
            file = open(nameRuta(path), "w+")
            timeout= time.time() + 20 #reestablece timeout en 20 segundos mas
            
def main():
    path= raw_input("ruta? (default: /opt/sixlabs/var/log):")
    creadorQry(path)

if __name__ == '__main__':
    main()
    
  


import datetime
import math
import random
import time
def getSeconds():
        return random.randint(0,9)
def timeFile():
        fecha = datetime.datetime.now()

        dia = str(fecha.day)
        mes = str(fecha.month)
        anio = str(fecha.year)
        hora = str(fecha.hour)
        minuto = str(fecha.minute)
        segundo= str(fecha.second)

        if len(dia) ==1:
            dia = "0" + dia

        if len(mes) ==1:
            mes = "0" + mes

        if len(hora) ==1:
            hora = "0" + hora

        if len(minuto) ==1:
            minuto = "0" + minuto
        if len(segundo) ==1:
            segundo = "0" + segundo

        return ""+anio+""+mes+""+dia+""+hora+minuto+segundo


def NONNEGINT(n):  #entrega un entero positivo, incluye cero
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return random.randint(range_start, range_end)



def namefile(ruta):
        if (len(ruta)==0): 
                return '/opt/sixlabs/var/log/DIA_1-MSU-'+timeFile() + '.csv' #default 
#Agregar cambio de archivo segun hora
        else:
                return ruta + '/DIA_1-MSU-'+timeFile() + '.csv'


def creadorDia(path):
  timeout = time.time() + 60  #se asigna variable de 60 segundos mas tiempo CPU
  while time.time() <= timeout: #condicion para que realice loop durante un minuto
    i = 0
    file = open(namefile(path), "w+")

    random.seed(3)
    while( i < 1):
        posint= str(NONNEGINT(random.randint(1,9)))#generador de numero entero positivo
        posint2=str(NONNEGINT(random.randint(1,9)))
        log= "324,"+posint+","+posint2

        print(log)
        file.write(log +'\n') #Escribe cada log en una linea
        i+=1

    file.close() #Cierra file
    time.sleep(5)
  print("finalizado logCreator")

def main():
    path= raw_input("ruta? (default: /opt/sixlabs/var/log:)")#ruta predeterminada de repo
    creadorDia(path)
    

if __name__ == '__main__':
    main()


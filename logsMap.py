import time
from LogCreatorDtm import creadorDtm
from LogCreatorQry import creadorQry
from LogcreatorMSUmap import creadorMap

#Creador de logs para dtm, qry y map
#Imprime logs en pantalla y a la vez los guarda en la ruta especificada o por defecto en /opt/sixlabs/var/log
#Archivos creados son con extension .dtm, .qry y .csv, respectivamente 
def creador():
    path= raw_input("Ruta para logs? (default: /opt/sixlabs/var/log):")
    print("Comenzando Dtm...\n")
    time.sleep(2)
    creadorDtm(path) #crea .dtm

    print("Comenzando Qry...\n")
    time.sleep(2)
    creadorQry(path) #crea .qry

    print("Comenzando MAP...\n")
    time.sleep(2)    
    creadorMap(path) #crea .csv

    print("Procesos terminados.\n")


def main():
    creador()

if __name__ == '__main__':
    main()
